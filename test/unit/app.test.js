const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const assert = require('assert');
const { TodoApp, Store } = require('../../src/app');

const dom = new JSDOM('<!doctype html><html><body></body></html>', {
    url: 'http://localhost:3000/index.html',
    storageQuota: 10000000,
});

global.localStorage = dom.window.localStorage;


// Test de la classe Store
describe('Store', () => {
    // Test du constructeur
    it('Doit initialiser le store avec des données vides si localStorage est vide', () => {
        const store = new Store();
        assert.deepStrictEqual(store.getData(), []);
    });

    // Test de la méthode saveData
    it('Doit sauvegarder les données dans localStorage', () => {
        const store = new Store();
        const testData = [{ id: 1, task: 'Test Task 1' }];
        store.data = testData;
        store.saveData();
        const storedData = JSON.parse(localStorage.getItem('tasks'));
        assert.deepStrictEqual(storedData, testData);
    });

    // Test de la méthode getData
    it('Doit obtenir les données du store', () => {
        const testData = [{ id: 1, task: 'Test Task 1' }];
        localStorage.setItem('tasks', JSON.stringify(testData));
        const store = new Store();
        assert.deepStrictEqual(store.getData(), testData);
    });
});

// Test de la classe TodoApp
describe('TodoApp', () => {
    // Test du constructeur
    it('Doit initialiser les tâches à partir du store', () => {
        const testData = [{ id: 1, task: 'Test Task 1' }];
        localStorage.setItem('tasks', JSON.stringify(testData));
        const store = new Store();
        const todoApp = new TodoApp(store);
        assert.deepStrictEqual(todoApp.getTasks(), testData);
    });

    // Test de la méthode addTask
    it('Doit ajouter une tâche et sauvegarder les données', () => {
        localStorage.removeItem('tasks');
        const store = new Store();
        const todoApp = new TodoApp(store);
        const newTask = { id: 2, task: 'New Task 2' };
        todoApp.addTask(newTask);
        assert.deepStrictEqual(todoApp.getTasks(), [newTask]);
    });

    // Test de la méthode removeTask
    it('Doit supprimer une tâche et sauvegarder les données', () => {
        const testData = [{ id: 1, task: 'Test Task 1' }, { id: 2, task: 'New Task 2' }];
        localStorage.setItem('tasks', JSON.stringify(testData));
        const store = new Store();
        const todoApp = new TodoApp(store);
        todoApp.removeTask(0);
        assert.deepStrictEqual(todoApp.getTasks(), [{ id: 2, task: 'New Task 2' }]);
    });

    // Test de la méthode getTasks
    it('Doit obtenir la liste des tâches', () => {
        const testData = [{ id: 1, task: 'Test Task 1' }];
        localStorage.setItem('tasks', JSON.stringify(testData));
        const store = new Store();
        const todoApp = new TodoApp(store);
        assert.deepStrictEqual(todoApp.getTasks(), testData);
    });
});



